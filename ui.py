import os.path
import sys
import traceback
from enum import Enum
from smtplib import SMTPAuthenticationError

from PyQt6 import QtWidgets
from PyQt6.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QGridLayout, QSpacerItem, QSizePolicy, \
    QLabel, QLineEdit, QStackedWidget, QFileDialog, QMessageBox, QRadioButton, QComboBox, QButtonGroup, QHBoxLayout, \
    QVBoxLayout, QTextEdit, QTableView, QTableWidget, QWidgetItem, QTableWidgetItem, QFrame, QProgressBar, \
    QErrorMessage, QScrollArea, QSizePolicy, QBoxLayout
from PyQt6.QtGui import QPixmap, QIntValidator, QIcon, QFont, QFontDatabase, QBrush, QColor
from PyQt6.QtCore import Qt, QUrl, pyqtSignal
from PyQt6.QtWebEngineWidgets import QWebEngineView
from _socket import gaierror

from attachment_handler import AttachmentHandler, CSVColumnAttachmentHandler, \
    SingleFileAttachmentHandler, NoneAttachmentHandler
from rechnungen_senden import RechnungenSenden

import csv, datetime, sys

from ui_helper import FilePicker, MontserratLabel, CheckboxWithInput


class Ui(QWidget):
    def __init__(self):
        super(Ui, self).__init__()

        #Define some fields
        self.__column_email_index = None
        self.__column_vorname_index = None
        self.__num_pages = 0
        self.__page = 1
        self.__attachment_handler = None
        self.__member_list_header = None
        self.__member_list = None
        self.__sender = None

        #Setup mainwindow
        self.setWindowIcon(QIcon("./img/juli_icon.png"))
        self.setWindowTitle("Rechnungen senden")

        # Define the Ui to switch between pages
        grid_layout_bottom = QGridLayout()
        self.__forward_button = QPushButton("Weiter")
        self.__back_button = QPushButton("Zurück")
        self.__back_button.setEnabled(False)
        spacer_bottom = QSpacerItem(1, 1, QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Minimum)
        grid_layout_bottom.addWidget(self.__back_button, 0, 0)
        grid_layout_bottom.addItem(spacer_bottom, 0, 1)
        grid_layout_bottom.addWidget(self.__forward_button, 0, 2)

        # Define the stage for the pages
        layout = QGridLayout()
        self.__pages = QStackedWidget()

        # Create and register the Pages
        self.__page1 = Page1()
        self.__register_page(self.__page1)
        self.__page2 = Page2()
        self.__register_page(self.__page2)
        self.__page3 = Page3()
        self.__register_page(self.__page3)
        self.__page4 = Page4()
        self.__register_page(self.__page4)
        self.__page5 = Page5()
        self.__register_page(self.__page5)
        self.__page6 = Page6()
        self.__register_page(self.__page6)
        self.__page7 = Page7()
        self.__register_page(self.__page7)

        self.__page7.set_forward_button(self.__forward_button)

        # Add the pages to the stage
        layout.addWidget(self.__pages, 0, 0)
        layout.addItem(grid_layout_bottom, 1, 0)
        self.setLayout(layout)

        # Set Signals
        self.__forward_button.clicked.connect(self.next_page)
        self.__back_button.clicked.connect(self.previous_page)

    def __register_page(self, page: QWidget):
        """
        Registers a new page to the Ui.

        :param page: The page to get registered
        """
        self.__pages.addWidget(page)
        self.__num_pages += 1

    def next_page(self):
        """
        Calls the next page.
        """
        # Logic, when you are leaving page 1
        if self.__page == 1:
            try:
                self.__sender = RechnungenSenden()
                login_data = self.__page1.get_login_data()  # gets the login-data to sign in to the mailserver
                #self.__sender.set_email_login_data(*login_data) # Try to login to the mailserver
            except gaierror as e:
                # Error, if the connection to the smtp-server fails
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Icon.Critical)
                message_box.setText("Verbindung zum SMTP-Server fehlgeschlagen!")
                message_box.setWindowTitle("Error")
                message_box.exec()
                return
            except SMTPAuthenticationError as e:
                # Error, if the Login fails
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Icon.Critical)
                message_box.setText("Login-Daten falsch!")
                message_box.setWindowTitle("Error")
                message_box.exec()
                return
            except ValueError:
                # Error, if input is empty
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Icon.Critical)
                message_box.setText("Bitte die Daten vollständig ausfüllen!")
                message_box.setWindowTitle("Error")
                message_box.exec()
                return
        # Logic, when you are leaving page 2
        elif self.__page == 2:
            if self.__page2.get_log_path() is None or self.__page2.get_log_path() == "":
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Icon.Critical)
                message_box.setText("Bitte Pfade auswählen")
                message_box.setWindowTitle("Error")
                message_box.exec()
                return
            try:
                self.__member_list_header, self.__member_list = self.__page2.get_members()  # Get the list with all the
                # members to send the E-Mail to. Data was imported from the csv-file
                self.__page3.set_csv_columns(tuple(self.__member_list_header))  # Adds the header of the csv-file to the
                # dropdown-menus of page 3
                self.__attachment_handler = self.__page2.get_selected_attachment_handler()\
                    (tuple(self.__member_list_header))  # Creates the selected attachment_handler
                self.__page3.set_attachment_handler_ui(self.__attachment_handler)  # Adds the attachment_handler-ui to
            except TypeError:
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Icon.Critical)
                message_box.setText("Bitte Pfade auswählen")
                message_box.setWindowTitle("Error")
                message_box.exec()
                return
            except AttributeError:
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Icon.Critical)
                message_box.setText("Bitte eine Option zur Zusweisung eines Anhangs auswählen")
                message_box.setWindowTitle("Error")
                message_box.exec()
                return

        elif self.__page == 3:
            if not self.__attachment_handler.input_sufficient():
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Icon.Critical)
                message_box.setText("Bitte Pfade auswählen")
                message_box.setWindowTitle("Error")
                message_box.exec()
                return
            self.__column_email_index = self.__page3.get_selected_email()
        elif self.__page == 4:
            subject, text, website_link, insta_link = self.__page4.get_input()
            self.__sender.set_mail_data(subject, text, website_link, insta_link)
            #self.__page5.set_preview_data(text, subject)
        elif self.__page == 5:
            header = self.__member_list_header + ["Anhang"]
            data = list()
            for member in self.__member_list:
                path_to_attachment = self.__attachment_handler.get_attachment(member)
                try:
                    open(path_to_attachment).close()
                    data.append(member + [path_to_attachment])
                except Exception as e:
                    data.append(member + ["File not found"])
            self.__page6.set_table_data(tuple(header), tuple(data))
        elif self.__page == 6:
            self.__page7.set_data(self.__member_list, self.__attachment_handler, self.__sender,
                                  self.__column_vorname_index, self.__column_email_index, self.__page2.get_log_path())

        # Opens the next page
        #if self.__page == 1:
        #    self.__back_button.setEnabled(True)
        elif self.__page == self.__num_pages:
            sys.exit(0)

        self.__pages.setCurrentIndex(self.__page)
        self.__page += 1

        if self.__page == self.__num_pages:
            self.__forward_button.setText("Beenden")

    def previous_page(self):
        """
        Calls the previous page.
        """
        if self.__page == 2:
            del self.__sender
            del self.__page2
            self.__page2 = Page2()
        elif self.__page == 3:
            self.__member_list_header = None
            self.__member_list = None
            self.__attachment_handler = None
            del self.__page3
            self.__page3 = Page3()
        elif self.__page == 4:
            self.__column_vorname_index = None
            self.__column_email_index = None
            self.__page4 = None
            self.__page4 = Page4()
        elif self.__page == 5:
            self.__page5 = None
            self.__page5 = Page5()
        elif self.__page == 6:
            self.__page6 = None
            self.__page6 = Page6()
        elif self.__page == 7:
            self.__page7 = None
            self.__page7 = Page7()

        if self.__page == self.__num_pages:
            self.__forward_button.setEnabled(True)

        self.__pages.setCurrentIndex(self.__page - 2)
        self.__page -= 1

        #if self.__page == 1:
        #    self.__back_button.setEnabled(False)


class Page1(QWidget):
    def __init__(self):
        """ Initializes the first page"""
        super(Page1, self).__init__()

        # Load the Logo
        logo = QPixmap("./img/AachenStadt.png")
        logo_label = QLabel()
        logo_label.setPixmap(logo)
        logo_label.setAlignment(Qt.AlignmentFlag.AlignHCenter)

        # Create the Form
        grid_layout_form = QGridLayout()

        label_email = MontserratLabel("E-Mail:")
        label_password = MontserratLabel("Passwort:")
        label_server = MontserratLabel("SMTP-Server:")
        label_port = MontserratLabel("Port:")

        self.__email = QLineEdit()
        self.__password = QLineEdit()
        self.__password.setEchoMode(QLineEdit.EchoMode.Password)
        self.__server = QLineEdit()
        self.__port = QLineEdit()
        only_int = QIntValidator()
        self.__port.setValidator(only_int)

        grid_layout_form.addWidget(label_email, 0, 0)
        grid_layout_form.addWidget(self.__email, 0, 1)
        grid_layout_form.addWidget(label_password, 1, 0)
        grid_layout_form.addWidget(self.__password, 1, 1)
        grid_layout_form.addWidget(label_server, 2, 0)
        grid_layout_form.addWidget(self.__server, 2, 1)
        grid_layout_form.addWidget(label_port, 3, 0)
        grid_layout_form.addWidget(self.__port, 3, 1)

        spacer = QSpacerItem(20, 40, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        # Create the first page
        page1 = QGridLayout()
        page1.addWidget(logo_label, 0, 0)
        page1.addItem(grid_layout_form, 1, 0)
        page1.addItem(spacer, 2, 0)
        self.setLayout(page1)


    def get_login_data(self):
        """Get the login data given in the form.
        :return: login data
        """
        if self.__email.text() == "" or self.__password.text() == "" or self.__server.text() == "" or \
                self.__port.text() == "":
            raise ValueError()
        return self.__email.text(), self.__password.text(), self.__server.text(), int(self.__port.text())


class Page2(QWidget):

    def __init__(self):
        """Initializes the second page"""
        super(Page2, self).__init__()

        font_id = QFontDatabase.addApplicationFont("./fonts/Montserrat-Regular.ttf")
        families = QFontDatabase.applicationFontFamilies(font_id)
        self.__montserrat_font = families[0]

        # Define some fields
        self.__attachment_handler = {}

        # Create the Widget to open the member-list-file (csv)
        csv_picker_label = MontserratLabel("Bitte wählen Sie die Mitgliederliste aus")
        csv_picker_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.__csv_picker = FilePicker(FilePicker.Mode.FILE, directory="C:\\Users\\erik-\\PycharmProjects\\julis-serienemails\\test_data")
        
        log_path_picker_label = MontserratLabel("Bitte Pfad für das Protokoll auswählen")
        log_path_picker_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.__log_path_picker = FilePicker(FilePicker.Mode.DIR)
        
        # Create UI to select the attachment handler
        attachment_handler_label = MontserratLabel("Wie soll der Anhang dem Empfänger zugeordnet werden?")
        attachment_handler_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.__attachment_handler_buttons = QButtonGroup()
        self.__attachment_hander_button_layout = QHBoxLayout()

        spacer = QSpacerItem(20, 40, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)

        # Register the attachment handlers
        self.register_attachment_handler(CSVColumnAttachmentHandler)
        self.register_attachment_handler(SingleFileAttachmentHandler)
        self.register_attachment_handler(NoneAttachmentHandler)


        # Put the page together
        layout = QGridLayout()
        layout.addWidget(log_path_picker_label, 0, 0)
        layout.addWidget(self.__log_path_picker, 1, 0)
        layout.addWidget(csv_picker_label, 2, 0)
        layout.addWidget(self.__csv_picker, 3, 0)
        layout.addWidget(attachment_handler_label, 4, 0)
        layout.addItem(self.__attachment_hander_button_layout, 5, 0)
        layout.addItem(spacer, 6, 0)
        self.setLayout(layout)

    def register_attachment_handler(self, handler_class):
        """Registers an attachment-handler to page 3.

        :param handler_class: The Class of the handler, that is to be registered
        """

        btn = QRadioButton(handler_class.get_name())
        btn.setFont(QFont(self.__montserrat_font, 12))
        self.__attachment_hander_button_layout.addWidget(btn)
        self.__attachment_handler_buttons.addButton(btn)
        self.__attachment_handler[handler_class.get_name()] = handler_class

    def get_log_path(self) -> str:
        return self.__log_path_picker.get_path()

    def get_members(self):
        """Reads the members from the csv-file.

        :return: header - The header of the csv-file; members - The members to send the email to
        """

        members = list()
        header = list()
        with open(self.__csv_picker.get_path(), encoding="utf-8-sig") as csv_file:
            reader = csv.reader(csv_file, delimiter=";")
            header = next(reader)
            for row in reader:
                members.append(row)
        return header, members

    def get_selected_attachment_handler(self):
        """Gets the attachment-handler, that is selected before

        :return: The Class of the attachment-handler
        """
        return self.__attachment_handler[self.__attachment_handler_buttons.checkedButton().text()]


class Page3(QWidget):
    def __init__(self):
        """Initializes the third Page"""
        super(Page3, self).__init__()

        label_email = MontserratLabel("Spalte, in der die Email-Adresse steht")
        label_email.setAlignment(Qt.AlignmentFlag.AlignCenter)

        line = QFrame()
        line.setObjectName(u"line")
        line.setFrameShape(QFrame.Shape.HLine)
        line.setFrameShadow(QFrame.Shadow.Sunken)

        spacer = QSpacerItem(20, 20, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.__dropdown_email = QComboBox()

        self.__layout = QVBoxLayout()
        self.__layout.addWidget(label_email)
        self.__layout.addWidget(self.__dropdown_email)
        self.__layout.addItem(spacer)
        self.__layout.addWidget(line)

        self.setLayout(self.__layout)



    def set_csv_columns(self, csv_columns: tuple):
        """Add the csv-header to the dropdown menus.

        :param csv_columns: The header of the csv-files
        """

        self.__dropdown_email.addItems(csv_columns)

    def get_selected_email(self):
        return self.__dropdown_email.currentIndex()

    def set_attachment_handler_ui(self, ui):
        """Adds the ui of the attachment-handler to Page 3

        :param ui: The attachment-handler
        """

        self.__layout.addWidget(ui)
        spacer = QSpacerItem(20, 40, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Expanding)
        self.__layout.addItem(spacer)


class Page4(QWidget):
    def __init__(self):
        super(Page4, self).__init__()

        self.__preview = EmailPreview()
        self.__preview_button = QPushButton("Preview")
        self.__preview_button.clicked.connect(self.__preview.show)

        self.__input_subject = QLineEdit()
        self.__input_text = QTextEdit()
        self.__input_website_link = CheckboxWithInput()
        self.__input_insta_link = CheckboxWithInput()
        self.__input_juli_logo = FilePicker(FilePicker.Mode.FILE, caption="Bilder (*.png)")
        self.__input_banner = FilePicker(FilePicker.Mode.FILE, caption="Bilder (*.png)")

        self.__input_text.textChanged.connect(self.updatePreview)

        self.__label_subject = MontserratLabel("Betreff")
        self.__label_text = MontserratLabel("E-Mail Inhalt")
        self.__label_website_link = MontserratLabel("Websiteverknüpfung hinzufügen")
        self.__label_insta_link = MontserratLabel("Instagram-Verküpfung hinzufügen")
        self.__label_juli_logo = MontserratLabel("Individuelles Julis-Logo")
        self.__label_banner = MontserratLabel("Individuelles Banner")

        # Erstelle ein vertikales Layout für das Fenster
        layout = QVBoxLayout()

        # Erstelle ein Widget, das in der ScrollArea angezeigt wird
        widget = QWidget()

        # Erstelle ein vertikales Layout für das Widget
        widgetLayout = QVBoxLayout()

        widgetLayout.addWidget(self.__preview_button)
        widgetLayout.addWidget(self.__label_subject)
        widgetLayout.addWidget(self.__input_subject)
        widgetLayout.addWidget(self.__label_text)
        widgetLayout.addWidget(self.__input_text)
        widgetLayout.addWidget(self.__label_website_link)
        widgetLayout.addWidget(self.__input_website_link)
        widgetLayout.addWidget(self.__label_insta_link)
        widgetLayout.addWidget(self.__input_insta_link)
        widgetLayout.addWidget(self.__label_juli_logo)
        widgetLayout.addWidget(self.__input_juli_logo)
        widgetLayout.addWidget(self.__label_banner)
        widgetLayout.addWidget(self.__input_banner)

        widget.setLayout(widgetLayout)

        # Erstelle eine ScrollArea und setze das Widget als Inhalt
        scroll_area = QScrollArea()
        scroll_area.setWidgetResizable(True)
        scroll_area.setWidget(widget)

        # Füge die ScrollArea dem Fenster-Layout hinzu
        layout.addWidget(scroll_area)

        self.setLayout(layout)

    def updatePreview(self):
        self.__preview.set_preview_data(self.__input_text.toPlainText().replace("\n", "<br>"))

    def get_input(self):
        return self.__input_subject.text(), self.__input_text.toPlainText().replace("\n", "<br>"), \
               self.__input_website_link.text(), self.__input_insta_link.text()


class EmailPreview(QWebEngineView):
    def __init__(self):
        super(EmailPreview, self).__init__()

        self.set_preview_data("")

    def closeEvent(self, event):
        # Verstecken Sie das Fenster, anstatt es zu schließen
        self.setVisible(False)
        event.ignore()

    def set_preview_data(self, text):
        with open("./email/email.html") as html_file:
            self.setHtml(html_file.read().format(Anrede="- Grußzeile -", Nachricht=text, image001="image001.png", image002="image002.png",
                                    image003="image003.png", image004="image004.png", image005="image005.png",
                                    Website="https://aachen.julis.de/", Instagram="https://www.instagram.com/julis_kv_aachen/"), baseUrl=QUrl.fromLocalFile(os.getcwd() + os.path.sep + "email" + os.path.sep + "email-bilder" + os.path.sep))


class Page5(QWidget):
    def __init__(self):
        super(Page5, self).__init__()

        self.__preview = QWebEngineView()
        self.__subject = MontserratLabel()
        self.__subject.setFixedHeight(20)

        self.__layout = QVBoxLayout()
        self.__layout.addWidget(self.__subject)
        self.__layout.addWidget(self.__preview)


    def set_preview_data(self, text, position, absender_name, subject):
        with open("./email/email.html") as html_file:
            self.__preview.setHtml(html_file.read().format(Anrede="- Grußzeile -", Nachricht=text,
                                    Name=absender_name, Titel=position, image001="image001.png", image002="image002.png",
                                    image003="image003.png", image004="image004.png", image005="image005.png",
                                    Website="https://aachen.julis.de/", Instagram="https://www.instagram.com/julis_kv_aachen/"), baseUrl=QUrl.fromLocalFile(os.getcwd() + os.path.sep + "email" + os.path.sep + "email-bilder" + os.path.sep))

        self.__subject.setText(f"Betreff: {subject}")
        self.setLayout(self.__layout)

class Page6(QWidget):
    def __init__(self):
        super(Page6, self).__init__()

        self.__beschreibung = MontserratLabel("Sende folgende E-Mails:")
        self.__table = QTableWidget()

        self.__layout = QVBoxLayout()
        self.__layout.addWidget(self.__beschreibung)
        self.__layout.addWidget(self.__table)
        self.setLayout(self.__layout)

    def set_table_data(self, header: tuple, data: tuple):
        self.__table.setRowCount(len(data) + 1)
        self.__table.setColumnCount(len(header))

        for i in range(0, len(header)):
            self.__table.setItem(0, i, QTableWidgetItem(header[i]))

        for i in range(0, len(data[0])):
            for j in range(0, len(data)):
                if data[j][-1] == "File not found":
                    bg = QBrush(QColor("red"))
                else:
                    bg = QBrush(QColor("white"))

                item = QTableWidgetItem(data[j][i])
                item.setBackground(bg)
                self.__table.setItem(j + 1, i, item)

        header = self.__table.horizontalHeader()

        for i in range(0, len(header)-1):
            header.setSectionResizeMode(i, QtWidgets.QHeaderView.ResizeMode.ResizeToContents)

        header.setSectionResizeMode(len(header)-1, QtWidgets.QHeaderView.ResizeMode.Stretch)

        self.__table.setEditTriggers(QtWidgets.QAbstractItemView.EditTrigger.NoEditTriggers)



class Page7(QWidget):
    def __init__(self):
        super(Page7, self).__init__()
        self.__log_path = None
        self.__forward_button = None
        self.__column_email_index = None
        self.__column_vorname_index = None
        self.__sender = None
        self.__attachment_handler = None
        self.__member_list = None

        title_label = MontserratLabel("Möchten Sie die E-Mails jetzt versenden?")
        title_label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.__progress_bar = QProgressBar()
        self.__progress_bar.setValue(0)

        self.__btn_send = QPushButton("Senden!")
        self.__btn_send.clicked.connect(self.__send_emails)

        layout = QVBoxLayout()
        layout.addWidget(title_label)
        layout.addWidget(self.__progress_bar)
        layout.addWidget(self.__btn_send)
        self.setLayout(layout)

    def set_data(self, member_list, attachment_handler, sender, column_vorname_index, column_email_index, log_path):
        self.__member_list = member_list
        self.__attachment_handler = attachment_handler
        self.__sender = sender
        self.__column_vorname_index = column_vorname_index
        self.__column_email_index = column_email_index
        self.__log_path = log_path

    def set_forward_button(self, button: QPushButton):
        self.__forward_button = button

    def __send_emails(self):
        old_stderr = sys.stderr
        date = datetime.datetime.now()
        with open(self.__log_path + os.path.sep + f"{date.year}-{date.month}-{date.day}_{date.timestamp()}.log", "w") as log:
            sys.stderr = log
            self.__btn_send.setEnabled(False)
            self.__forward_button.setEnabled(False)
            for i in range(0, len(self.__member_list)):
                path_to_attachment = self.__attachment_handler.get_attachment(self.__member_list[i])
                try:
                    open(path_to_attachment).close()
                    log.writelines((f"Sending Email to {self.__member_list[i]} with Attachment {path_to_attachment}\r\n"))
                    self.__sender.send_mail("Hi", self.__member_list[i][self.__column_vorname_index],
                                            self.__member_list[i][self.__column_email_index], path_to_attachment)
                    self.__progress(int((i+1)*100/len(self.__member_list)))
                except FileNotFoundError:
                    log.writelines(f"Didn't sent Mail to {self.__member_list[i]}. Attachment not found.\r\n")
                except Exception:
                    traceback.print_exc(file=log)
                    log.write("\r\n")
            self.__forward_button.setEnabled(True)

        sys.stderr = old_stderr

    def __progress(self, progress: int):
        print(f"progress:{progress}")
        self.__progress_bar.setValue(progress)