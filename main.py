import os
import traceback

from PyQt6.QtCore import QUrl
from PyQt6.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QErrorMessage

from rechnungen_senden import RechnungenSenden
from ui import Ui

# Only needed for access to command line arguments
import sys

# You need one (and only one) QApplication instance per application.
# Pass in sys.argv to allow command line arguments for your app.
# If you know you won't use command line arguments QApplication([]) works too.
from ui_helper import QTextEditor

app = QApplication(sys.argv)

error_msg = QErrorMessage()
def excepthook(exc_type, exc_value, exc_tb):
    tb = "".join(traceback.format_exception(exc_type, exc_value, exc_tb))
    error_msg.showMessage(tb)

window = QTextEditor()
#window = Ui()
sys.excepthook = excepthook
window.show()

# Start the event loop.
app.exec()


