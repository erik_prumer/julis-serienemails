from enum import Enum

from PyQt6.QtGui import QFontDatabase, QFont, QTextCharFormat
from PyQt6.QtWidgets import QWidget, QLineEdit, QPushButton, QGridLayout, QFileDialog, QLabel, QStyle, QCheckBox, \
    QHBoxLayout, QVBoxLayout, QTextEdit


class QTextEditor(QWidget):
    def __init__(self):
        super(QTextEditor, self).__init__()

        layout = QVBoxLayout(self)
        menu_layout = QHBoxLayout()

        self.__input = QTextEdit()

        font_id = QFontDatabase.addApplicationFont("./fonts/Montserrat-Regular.ttf")
        families = QFontDatabase.applicationFontFamilies(font_id)
        montserrat_font = families[0]

        self.__input.setFont(QFont("Verdana", 12))

        btn_bold = QPushButton("Bold")
        btn_underline = QPushButton("Underline")
        btn_italic = QPushButton("Cursive")
        btn_hyperlink = QPushButton("Hyperlink")

        btn_bold.clicked.connect(self.__bold_action)
        btn_underline.clicked.connect(self.__underline_action)
        btn_italic.clicked.connect(self.__italic_action)

        menu_layout.addWidget(btn_bold)
        menu_layout.addWidget(btn_underline)
        menu_layout.addWidget(btn_italic)
        menu_layout.addWidget(btn_hyperlink)

        layout.addLayout(menu_layout)
        layout.addWidget(self.__input)

    def __bold_action(self):
        cursor = self.__input.textCursor()
        format = QTextCharFormat()
        if not cursor.charFormat().font().bold():
            format.setFontWeight(QFont.Weight.Bold)
        else:
            format.setFontWeight(QFont.Weight.Normal)
        cursor.mergeCharFormat(format)

        print(self.__input.document().toHtml())


    def __underline_action(self):
        cursor = self.__input.textCursor()
        format = QTextCharFormat()
        if not cursor.charFormat().font().underline():
            format.setUnderlineStyle(QTextCharFormat.UnderlineStyle.SingleUnderline)
        else:
            format.setUnderlineStyle(QTextCharFormat.UnderlineStyle.NoUnderline)
        cursor.mergeCharFormat(format)

    def __italic_action(self):
        cursor = self.__input.textCursor()
        format = QTextCharFormat()
        format.setFontItalic(True)
        if not cursor.charFormat().font().italic():
            format.setFontItalic(True)
        else:
            format.setFontItalic(False)
        cursor.mergeCharFormat(format)

    def __hyperlink_action(self):
        pass

class MontserratLabel(QLabel):
    def __init__(self, text="", font_size=12, color="black"):
        super(MontserratLabel, self).__init__(text)

        font_id = QFontDatabase.addApplicationFont("./fonts/Montserrat-Regular.ttf")
        families = QFontDatabase.applicationFontFamilies(font_id)
        montserrat_font = families[0]

        self.setFont(QFont(montserrat_font, font_size))
        self.setStyleSheet(f"color: {color}")

class CheckboxWithInput(QWidget):
    def __init__(self):
        super(CheckboxWithInput, self).__init__()

        layout = QHBoxLayout()

        self.__check_box = QCheckBox()
        self.__input = QLineEdit()
        self.__input.setEnabled(False)

        self.__check_box.stateChanged[int].connect(self.__checkbox_state_changed)

        layout.addWidget(self.__check_box)
        layout.addWidget(self.__input)

        self.setLayout(layout)

    def __checkbox_state_changed(self, state):
        self.__input.setEnabled(bool(state))
        self.__input.setText("")

    def text(self):
        return self.__input.text() if self.__check_box.isEnabled() else None

    def isEnabled(self) -> bool:
        return self.__check_box.isEnabled()

        


class FilePicker(QWidget):
    def __init__(self, mode, caption="Open file", directory="c:\\", filter="CSV-Dateien (UTF-8) (*.csv)"):
        super(FilePicker, self).__init__()

        self.__path = None

        self.__mode = mode
        self.__caption = caption
        self.__directory = directory
        self.__filter = filter

        self.__path_input = QLineEdit()
        self.__path_input.setEnabled(False)
        self.__open_file_dialog = QPushButton("...")
        self.__open_file_dialog.setFixedWidth(50)
        self.__open_file_dialog.clicked.connect(self.__call_file_dialog)

        layout = QGridLayout()
        layout.addWidget(self.__path_input, 0, 0)
        layout.addWidget(self.__open_file_dialog, 0, 1)
        self.setLayout(layout)

    def __call_file_dialog(self):
        if self.__mode == self.Mode.FILE:
            (self.__path, _) = QFileDialog.getOpenFileName(self, self.__caption, self.__directory, self.__filter)
        else:
            self.__path = str(
                QFileDialog.getExistingDirectory(self, caption=self.__caption, directory=self.__directory))
        self.__path_input.setText(self.__path)

    def get_path(self):
        return self.__path

    class Mode(Enum):
        FILE = 0
        DIR = 1
