import imghdr
import logging
import mimetypes
import smtplib, ssl
from email.message import EmailMessage
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from socket import gaierror
from smtplib import SMTPAuthenticationError
import traceback

class RechnungenSenden:

    def __init__(self):
        self.__website_link = str()
        self.__insta_link = str()
        self.__position = str()
        self.__name = str()
        self.__message = str()
        self.__subject = str()
        self.__smtp_server = str()
        self.__port = int()
        self.__password = str()
        self.__sender_email = str()
        self.__connection = None

    def __del__(self):
        try:
            self.__connection.close()
        except AttributeError:
            pass

    def send_mail(self, anrede: str, vorname: str, email: str, attachment_dir: str):
        message = EmailMessage()
        message["From"] = self.__sender_email
        message["Subject"] = self.__subject
        message["To"] = email

        message.make_related()

        with open("./email/email.html") as html_file:
            message.add_related(html_file.read().format(Anrede=f"{anrede} {vorname}",
                                                                          Nachricht=self.__message,
                                                                          Name=self.__name, Titel=self.__position,
                                                                          image001="cid:image001",
                                                                          image002="cid:image002",
                                                                          image003="cid:image003",
                                                                          image004="cid:image004",
                                                                          image005="cid:image005",
                                                                          Website=self.__website_link,
                                                                          Instagram=self.__insta_link
                                                                          ), 'html')

        message.add_related(
            f"{anrede} {vorname},\r\n{self.__message}\r\n\r\nViele Grüße\r\n{self.__name}\r\n{self.__position}",
            "plain")

        for i in range(1, 6):
            with open(f"./email/email-bilder/image00{i}.png", "rb") as image:
                message.add_related(image.read(), "image", "png", cid=f"image00{i}")

        with open(attachment_dir, "rb") as rechnung:
            message.add_related(rechnung.read(), maintype='application', subtype='pdf', filename='Rechnung.pdf')

        text = message.as_string()
        self.__connection.sendmail(self.__sender_email, email, text)

    def set_email_login_data(self, email, password, smtp_server, port=465):
        self.__sender_email = email
        self.__password = password
        self.__smtp_server = smtp_server
        self.__port = port
        self.__connect()

    def set_mail_data(self, subject, message, website_link, insta_link):
        self.__subject = subject
        self.__message = message
        self.__insta_link = insta_link
        self.__website_link = website_link

    def __connect(self):
        if self.__port == 465:
            context = ssl.create_default_context()
            self.__connection = smtplib.SMTP_SSL(self.__smtp_server, self.__port, context=context)
        else:
            self.__connection = smtplib.SMTP(self.__smtp_server, self.__port)
            self.__connection.ehlo()
            self.__connection.starttls()

        self.__connection.login(self.__sender_email, self.__password)
