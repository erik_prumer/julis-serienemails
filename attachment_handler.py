import os.path
from abc import abstractmethod

import PyPDF2
from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QWidget, QComboBox, QGridLayout, QLabel, QVBoxLayout

from ui_helper import FilePicker, MontserratLabel


class AttachmentHandler(QWidget):
    def __init__(self, csv_header: tuple):
        super(AttachmentHandler, self).__init__()
        self.__csv_header = csv_header

    @abstractmethod
    def get_attachment(self, person: tuple) -> str:
        """
        Get the path of the attachment associated to a specific person

        :rtype: str
        :param person: Data of a person to get the attachment to
        """
        pass

    @abstractmethod
    def input_sufficient(self) -> bool:
        pass

    @staticmethod
    @abstractmethod
    def get_name() -> str:
        """Returns the unique name of the attachment-handler"""
        pass


class CSVColumnAttachmentHandler(AttachmentHandler):
    """
    Get the specific Filename from a specific column from the csv-file
    """

    def input_sufficient(self) -> bool:
        return False if self.__base_path_picker.get_path() is None or self.__base_path_picker.get_path() == "" else True

    def __init__(self, csv_header: tuple):
        super(CSVColumnAttachmentHandler, self).__init__(csv_header)

        self.__label_file_column = MontserratLabel("Bitte Spalte des Anhangs auswählen")
        self.__label_file_column.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.__dropdown_file_column = QComboBox()
        self.__label_base_path = MontserratLabel("Bitte Ordner auswählen, der die Anhänge enthält")
        self.__label_base_path.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.__dropdown_file_column.addItems(csv_header)
        self.__base_path_picker = FilePicker(FilePicker.Mode.DIR)

        layout = QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__label_file_column, 0, 0)
        layout.addWidget(self.__dropdown_file_column, 1, 0)
        layout.addWidget(self.__label_base_path, 2, 0)
        layout.addWidget(self.__base_path_picker, 3, 0)
        self.setLayout(layout)

    def get_attachment(self, person: tuple) -> str:
        print(f"{self.__base_path_picker.get_path()}/{person[self.__dropdown_file_column.currentIndex()]}")
        return f"{self.__base_path_picker.get_path()}/{person[self.__dropdown_file_column.currentIndex()]}"

    @staticmethod
    def get_name():
        return "Individueller Anhang"

class SingleFileAttachmentHandler(AttachmentHandler):
    """
    Send a single File to all recipients
    """

    def input_sufficient(self) -> bool:
        return False if self.__path_picker.get_path() is None else True

    def __init__(self, csv_header: tuple):
        super().__init__(csv_header)
        self.__label_path = MontserratLabel("Bitte Anhang auswählen")
        self.__label_path.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.__path_picker = FilePicker(FilePicker.Mode.FILE)

        layout = QGridLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.__label_path, 0, 0)
        layout.addWidget(self.__path_picker, 1, 0)
        self.setLayout(layout)

    def get_attachment(self, person: tuple) -> str:
        return self.__path_picker.get_path()

    @staticmethod
    def get_name():
        return "Individueller Anhang"

class NoneAttachmentHandler(AttachmentHandler):
    """
    No Attachment
    """

    def input_sufficient(self) -> bool:
        return True

    def __init__(self, csv_header: tuple):
        super().__init__(csv_header)

    def get_attachment(self, person: tuple) -> str:
        return ""

    @staticmethod
    def get_name():
        return "Kein Anhang"